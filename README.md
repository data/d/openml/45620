# OpenML dataset: Weather

https://www.openml.org/d/45620

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The weather problem is a tiny dataset that we will use repeatedly to illustrate machine learning methods. Entirely fictitious, it supposedly concerns the conditions that are suitable for playing some unspecified game. In general, instances in a dataset are characterized by the values of features, or attributes, that measure different aspects of the instance. In this case there are four attributes: outlook, temperature, humidity, and windy. The outcome is whether to play or not.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45620) of an [OpenML dataset](https://www.openml.org/d/45620). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45620/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45620/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45620/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

